package com.example.akankagarwal.hypertrack;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;


import com.hypertrack.core_android_sdk.HyperTrackCore;
import com.hypertrack.core_android_sdk.permissions.LocationPermissionCallback;


import java.sql.Struct;

public class MainActivity extends AppCompatActivity {

    String deviceId = "";
    final String PUBLISHER_KEY = "pk_4a377a4e30ac6cff89bf010c4e6556e08f0a7000";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RequestLocationPermission();
    }

    public void StartTracking(View view) {
        if (!HyperTrackCore.checkLocationPermission(getApplicationContext())){
            RequestLocationPermission();
        }
        boolean isTracking = HyperTrackCore.isTracking();
        String message = "";
        if (isTracking){
            message = deviceId + " is being tracked";
        }else
        {
            HyperTrackCore.resumeTracking();
            message = "Started tacking " + deviceId + " again";
        }
        Utils.ShowToast(getApplicationContext(), message);
    }

    public void StopTracking(View view) {
        HyperTrackCore.pauseTracking();

        if (!HyperTrackCore.isTracking()){
            Utils.ShowToast(getApplicationContext(), "Tacking has been paused!!");
        }
        else
            Utils.ShowToast(getApplicationContext(), "Something went wrong!!");
    }

    private void InitializeTracker(){
        // check if this is the first time initiation
        if (deviceId == null || deviceId.isEmpty()){
            HyperTrackCore.initialize(getApplicationContext(), PUBLISHER_KEY);
            deviceId = HyperTrackCore.getDeviceId();
        }
    }

    private void RequestLocationPermission(){
        HyperTrackCore.requestLocationPermissions(this, new LocationPermissionCallback() {
            @Override
            public void onLocationPermissionGranted() {
                Utils.ShowToast(getApplicationContext(), "Happy Tracking!!");
                InitializeTracker();
            }

            @Override
            public void onLocationPermissionDenied() {
                Utils.ShowToast(getApplicationContext(), "Please enable the location");
            }
        });
    }
}
